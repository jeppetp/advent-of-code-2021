f = open('data')
size = [1000,1000]


pairs = []

for l in f:
    pairs.append([k.split(',') for k in l.split(' -> ')])

pairs = pairs[:-1]

xs = [[] for _ in range(1000)]
for i in range(1000):
    #print(i)
    for p in pairs:
        #print(p[0][0])
        if int(p[0][0]) == i and p[0][0] == p[1][0]:
            xs[i].append(p)


ys = [[] for _ in range(1000)]
for j in range(1000):
    #print(i)
    for p in pairs:
        #print(p)
        #print(p[0][1])
        if int(p[0][1]) == j and int(p[0][1]) == int(p[1][1]):
            #print(p)
            ys[j].append(p)

diag1 = [[] for _ in range(1999)]
diag2 = [[] for _ in range(1999)]


for i in range(1999):
    for p in pairs:
        if int(p[0][0])+ int(p[0][1])==int(p[1][0])+int(p[1][1])==i:
            diag1[i].append(p)

for i in range(1999):
    for p in pairs:
        if int(p[0][0]) - int(p[0][1]) + 999==int(p[1][0])-int(p[1][1]) + 999 == i:
            diag2[i].append(p)

def on_line(line, point):
    minx = min(int(line[0][0]), int(line[1][0]))
    miny = min(int(line[0][1]), int(line[1][1]))
    maxx = max(int(line[0][0]), int(line[1][0]))
    maxy = max(int(line[0][1]), int(line[1][1]))
    return (minx  <= point[0] <= maxx) and (miny <= point[1] <= maxy)

numpoints = 0
for x in range(1000):
    for y in range(1000):
        numlines = 0
        for line in xs[x]:
            if on_line(line,[x,y]):
                numlines += 1
        for line in ys[y]:
            if on_line(line,[x,y]):
                numlines += 1
        for line in diag1[x+y]:
            if on_line(line,[x,y]):
                numlines += 1
        for line in diag2[x-y+999]:
            if on_line(line,[x,y]):
                numlines += 1
        if numlines > 1:
            numpoints += 1

print(f"numpoints: {numpoints}")

#board = []
#for x in range(1000):
#    board.append([0 for a in range(1000)])


#print(board)
#print(len(board))
#corner = [0,0]

#for l in pairs[:-1]:
#    print(l)
#    board_updated = []
#    for x in range(1000):
#        row_updated = []
#        for y in range(1000):
#            if on_line(l, [x,y]):
#                row_updated.append(board[x][y] + 1)
#            else:
#                row_updated.append(board[x][y])
#        board_updated.append(row_updated)
#print(board_updated)
#
#print(corner)

