(use 'clojure.java.io)

;;Get data as list of strings

(defn get-lines [fname]
	(with-open [r (reader fname)]
		(doall (line-seq r))
	)
)

(def lines (get-lines (first *command-line-args*)))

;;functions for each direction of movement

(defn move_up [pos dist]
	[(nth pos 0) (- (nth pos 1) dist)]
)

(defn move_down [pos dist]
	[(nth pos 0) (+ (nth pos 1) dist)]
)

(defn move_forward [pos dist]
	[(+ dist (nth pos 0)) (nth pos 1)]
)

(defn move [pos text]
	(case (first text)
		\u (move_up pos (Integer/parseInt (apply str (drop 3 text))))
		\d (move_down pos (Integer/parseInt (apply str (drop 5 text))))
		\f (move_forward pos (Integer/parseInt (apply str (drop 8 text))))
	)
)
;;recursively go though lines and update position accordingly
(defn move_all [pos data]
	(if (= (count data) 0)
		pos
		(move_all (move pos (first data)) (rest data)) 
	)
)

;;print product of height and depth (take 2 pos)

(println (apply * (take 2 (move_all [0 0 0] lines))))
