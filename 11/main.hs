import System.IO
import Control.Monad
import Data.Dynamic
import Data.Char
import Data.List
import Data.Maybe

strToIntList :: String -> [Int]
strToIntList = map digitToInt

legalPoint :: [[Int]] -> (Int, Int) -> Bool
legalPoint a p = ((fst p) < length a) 
                    && ((fst p) >= 0) 
                    && ((snd p) < (length $a !! 0))
                    && ((snd p) >= 0)

neighbors :: [[Int]] -> (Int, Int) -> [(Int, Int)]
neighbors a p = filter (legalPoint a) [((fst p) - 1, (snd p) - 1), 
                                              ((fst p) - 1, (snd p)), 
                                              ((fst p) - 1, (snd p) + 1), 
                                              ((fst p), (snd p) - 1), 
                                              ((fst p), (snd p) + 1), 
                                              ((fst p) + 1, (snd p) - 1), 
                                              ((fst p) + 1, (snd p)), 
                                              ((fst p) + 1, (snd p) + 1)]

increment :: Int -> [Int] -> [Int]
increment 0 (y:ys) = (if y /= 0 then y + 1 else 0):ys
increment i (y:ys) = y:(increment (i-1) ys)


-- Replace value at index arg1 in arg3 with value arg2
replace :: Int -> a -> [a] -> [a]
replace 0 x (y:ys) = x:ys
replace i x (y:ys) = y:(replace (i-1) x ys)

-- Increment single value of 2d-list
inc_2d :: Int -> Int -> [[Int]] -> [[Int]]
inc_2d row col xs =
  replace row (increment col $xs !! row) xs

-- Increment values at indices from arg1 in 2d-array arg2
inc_mult :: [(Int, Int)] -> [[Int]] -> [[Int]]
inc_mult [] board = board
inc_mult (y:ys) board = inc_mult ys $inc_2d (fst y) (snd y) board

zip' = zipWith zip

bools_to_coords :: [[Bool]] -> [(Int, Int)]
bools_to_coords bools = 
  let coords = [[(x,y) | y <- [0..(length bools - 1)]] | x <- [0..((length (bools !! 0)) - 1)]] in
    map (snd) $ filter (\x -> fst x) $ foldr (++) [] $ zip' bools coords 
find_matches :: [[a]] -> (a -> Bool) -> [(Int, Int)]
find_matches listlist condition =
  bools_to_coords $ map (map condition) listlist

increment_all :: [[Int]] -> [[Int]]
increment_all a = map (map ((+) 1)) a

update_stack :: [[Int]] -> [(Int, Int)] -> [(Int, Int)] -> [(Int, Int)]
update_stack board a b = a ++ (foldr (++) [] (map (\x -> neighbors board x) b))

reset_10s :: [[Int]] -> [[Int]]
reset_10s a = map (map (\x -> if (x >= 10) then 0 else x)) a

single_step :: [[Int]] -> ([[Int]], Int)
single_step a =
  let flash_coords = find_matches a (\x -> x >= 10) in
  let inc_coords = update_stack a [] flash_coords in
      (inc_mult inc_coords $ reset_10s a, length flash_coords)

inner_loop :: [[Int]] -> ([[Int]], Int)
inner_loop a = 
  let next = single_step a in
      if fst next == a
         then (a, 0)
      else
        let retval = inner_loop $ fst next in
          (fst retval, (snd next) + (snd retval))

outer_loop :: ([[Int]], Int) -> ([[Int]], Int)
outer_loop start =
  let inner = inner_loop $ increment_all $fst start in
      (fst inner, snd inner + (snd start))

main = do
  filetxt <- readFile "data"
  
  let d = lines filetxt :: [String]

  let height = length d
  let width = length $ d !! 0

  let coords = [(x, y) | x <- [0..height-1], y <- [0..width -1]]  
  let start = map strToIntList d
  let sequence = iterate outer_loop (start, 0)
  
  print $ snd $ sequence !! 100
  print $ fromJust $ findIndex (\x -> (fst x == [[0 | x <- [0..9]] | y <- [0..9]])) sequence
