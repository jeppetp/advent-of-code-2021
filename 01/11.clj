(use 'clojure.java.io)

;; get data as list of strings

(defn get-lines [fname]
	(with-open [r (reader fname)]
		(doall (line-seq r))
	)
)

(def lines (get-lines (first *command-line-args*)))

;; get index 0 and 1 as integers

(defn seq1 [data] (Integer/parseInt (first data))) 

(defn seq2 [data] (Integer/parseInt (nth data 1)))


(defn num_inc [data] 
	(if (= (count data) 1)
		0
		(if (< (seq1 data) (seq2 data))
			(+ 1 (num_inc (rest data)))
			(num_inc (rest data))
		)
	)
)
		
(print (num_inc lines))
(print "\n")


