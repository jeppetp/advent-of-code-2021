(use 'clojure.java.io)

;;Get data as list of strings

(defn get-lines [fname]
	(with-open [r (reader fname)]
		(doall (line-seq r))
	)
)

(def lines (get-lines (first *command-line-args*)))

;;Get index 0 and 4 as integers

(defn seq1 [data] (Integer/parseInt (first data))) 

(defn seq2 [data] (Integer/parseInt (nth data 3)))



(defn num_inc [data] 
	(if (= (count data) 3)
		0
		(if (< (seq1 data) (seq2 data))
			(+ 1 (num_inc (rest data)))
			(num_inc (rest data))
		)
	)
)
		
(print (num_inc lines))
(print "\n")


