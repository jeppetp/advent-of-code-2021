from typing import List

f = open('data')
lines = [[a for a in l][:-1] for l in f]

parentheses = [["(",")"], ["[","]"],["{","}"],["<",">"]]

scores = {")":3,"]":57,"}":1197,">":25137}

error_elements: List[str] = []
while lines:
    foundError = False
    line = lines.pop()
    completeline = line
    stack: List[str] = []
    while line and not foundError:
        nextel = line[0]
        line = line[1:]
        if nextel in [a[0] for a in parentheses]:
            if nextel == "(":
                appendel = ")"
            else:
                appendel = chr(ord(nextel) + 2)
            stack.append(appendel)
        else:
            stackpop = stack.pop()
            if not nextel == stackpop:
                foundError = True
                error_elements.append(nextel)

result = 0
for er in error_elements:
    result += scores[er]
print(result)
