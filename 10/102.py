from typing import List

f = open('data')
lines = [[a for a in l][:-1] for l in f]

parentheses = [["(",")"], ["[","]"],["{","}"],["<",">"]]

scores = {")":1,"]":2,"}":3,">":4}

errorscores: List[int] = []

while lines:
    errorscore = 0
    foundError = False
    line = lines.pop()
    completeline = line
    stack: List[str] = []
    while line and not foundError:
        nextel = line[0]
        line = line[1:]
        if nextel in [a[0] for a in parentheses]:
            if nextel == "(":
                appendel = ")"
            else:
                appendel = chr(ord(nextel) + 2)
            stack.append(appendel)
        else:
            stackpop = stack.pop()
            if nextel == stackpop:
                ...
            else:
                #print(f"ERROR in line {i}: expected {stackpop} but found {nextel} instead")
                foundError = True
    if not foundError:
        while stack:
            er = stack.pop()
            errorscore *= 5
            errorscore += scores[er]
        errorscores.append(errorscore)
print(sorted(errorscores)[(len(errorscores)-1)//2])
