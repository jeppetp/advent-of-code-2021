(use 'clojure.java.io)

;;Get data as list of strings

(defn get-lines [fname]
	(with-open [r (reader fname)]
		(doall (line-seq r))
	)
)

(defn get_bit_list [line]
	(conj (map #(Character/digit % 2) (seq line)) 1)
)

(def lines (get-lines (first *command-line-args*)))
;;(defn int_lines (map Integer/parseInt lines))
(defn find_average [data]
	(if (= 0 (count data))
		[0 0 0 0 0 0 0 0 0 0 0 0 0]
		(map + (find_average (rest data)) (get_bit_list (first data)) )
	)
)

;;(println (get_bit_list (first lines)))
(println (find_average lines))
;;(run! println lines)
;;(run! println intlist)
