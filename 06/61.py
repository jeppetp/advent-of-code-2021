f = open('data')

fishes = [0 for _ in range(300)]

ls = map(int, f.read().split(","))

for d in ls:
    fishes[d] = fishes[d]+1

def sum_fish(offset, sim):
    sum = 0
    for i in range(9):
        #print(sim[offset+i])
        sum += sim[offset + i]
    return sum

for n in range(80):
    fishes[6+n+1] += fishes[n]
    fishes[8+n+1] += fishes[n]

print(sum_fish(80, fishes))

