from typing import List, Set, Tuple
from functools import reduce
from itertools import product

f = open('data')
lines = f.read().split("\n")[:-1]

def neighbors(x: int, y: int, data: List[str]):
    possible = {(x-1, y), (x+1,y), (x,y-1), (x,y+1)}
    return {
        a
        for a in possible
        if (
            a[0] not in {-1, len(data)}
            and a[1] not in {-1, len(data[0])}
        )}

unvisited = {(x,y) for (x,y) in product(*[range(100)]*2) if lines[x][y] != "9"}

basin_sizes: Set[int] = set()

while unvisited:
    basin_size = 1
    rand_el = unvisited.pop()
    stack: Set[Tuple[int, int]] = (neighbors(*rand_el, lines) & unvisited)
    while stack:
        to_be_removed = stack & unvisited
        for s in to_be_removed:
            unvisited.remove(s)
            basin_size += 1
        current = stack.pop()
        stack = stack.union(neighbors(*current, lines) & unvisited)
    basin_sizes.add(basin_size)
print(reduce((lambda a, b: a*b), sorted(basin_sizes)[-3:]))
