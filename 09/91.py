f = open('data')

lines = f.read().split("\n")[:-1]

def neighbors(x,y, data):
    ret = []
    if x != 0:
        ret.append([x-1, y])
    if x != len(data)-1:
        ret.append([x+1, y])
    if y != 0:
        ret.append([x, y-1])
    if y != len(data)-1:
        ret.append([x, y+1])
    return ret

def check_minima(x,y, data):
    neigh = neighbors(x,y, data)
    ret = True
    for i in neigh:
        if data[x][y] >= data[i[0]][i[1]]:
            ret = False
    if ret:
        return data[x][y]
    else:
        return -1

result = 0

for i in range(len(lines)):
    for j in range(len(lines[0])):
        if check_minima(i,j,lines) != -1:
            result += int(lines[i][j])+1

print(result)
