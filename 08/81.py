from rich import print

def parse_number(letters):
    if len(letters) == 2:
        print("len 2")
        ret = 1
    elif len(letters) == 3:
        ret = 7
    elif len(letters) == 4:
        ret = 4
    elif len(letters) == 7:
        ret = 8
    else:
        ret = 0
    print(letters, ret)
    return int(ret != 0)

with open('data') as f:
    outputs = [line.split(' | ')[1][:-1] for line in f]
    
    print(sum(sum(parse_number(n) for  n in line.split()) for line in outputs))

    
