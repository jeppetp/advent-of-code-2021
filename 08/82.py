from itertools import permutations
from functools import partial

display_0 = {0,1,2,4,5,6}
display_1 = {2,5}
display_2 = {0,2,3,4,6}
display_3 = {0,2,3,5,6}
display_4 = {1,2,3,5}
display_5 = {0,1,3,5,6}
display_6 = {0,1,3,4,5,6}
display_7 = {0,2,5}
display_8 = {0,1,2,3,4,5,6}
display_9 = {0,1,2,3,5,6}

identity_perm = [n for n in range(10)]

correct_numbers = [display_0, display_1, display_2, display_3, display_4, display_5, display_6, display_7, display_8, display_9]

def check_data(data):
    return all(c in data for c in correct_numbers)

def set_to_int(segments):
    ret = ""
    for i in range(4):
        for j in range(10):
            if correct_numbers[j] == segments[i]:
                ret += str(j)
    return int(ret)

def permute_string(data, permutation):
    #print(f"permuting {data} with permutation {permutation}")
    ret = []
    for dat in data:
        word_data = set()
        for d in dat:
            word_data.add(permutation[ord(d) - 97])
        ret.append(word_data)
    return ret

f = open('data')
datavalues = []
outputs = []


for line in f:
    l = line.split(' | ')
    datavalues.append(l[0].split())
    outputs.append(l[1].split())

datadata = zip(datavalues, outputs)

sum = 0
for datadatadata in datadata:
        for i in permutations(range(7)):
            if check_data(permute_string(datadatadata[0], i)):
                sum += set_to_int(permute_string(datadatadata[1],i))

print(sum)
