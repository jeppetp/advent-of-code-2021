f = open('data')

longstring = f.read()

f.close()

splitstring = longstring.split('\n\n')

nums = splitstring[0].split(",")
loserfound = False
data_boards = []
data_markeds = []
for l in splitstring[1:-1]:
    board = []
    marked = []
    lines = l.split('\n')
    for lin in lines:
        marked.append([0,0,0,0,0])
        board.append(lin.split())
    data_markeds.append(marked)
    data_boards.append(board)

def check_rows(marked):
    won = False
    for i in range(len(marked)):
        wonrow = 1
        for j in range(len(marked[i])):
            if marked[i][j] == 0:
                wonrow = 0
        if wonrow:
            won = True
    return won

def check_cols(marked):
    won = False
    for i in range(len(marked)):
        wonrow = 1
        for j in range(len(marked[i])):
            if marked[j][i] == 0:
                wonrow = 0
        if wonrow:
            won = True
    return won

def check_plate(marked):
    if check_rows(marked) or check_cols(marked):
        pass
        #print(f"board: {marked}, rows: {check_rows(marked)}, cols: {check_cols(marked)}")
    return check_rows(marked) or check_cols(marked)

def check_winner(markeds):
    j = None
    for i in range(len(markeds)):
        if check_plate(markeds[i]):
            j = i
    return j

def check_loser(markeds):
    losers = 0
    last_loser = None
    for i in range(len(markeds)):
        if not check_plate(markeds[i]):
            losers += 1
            last_loser = i
    if losers == 1:
        return last_loser
    elif losers == 0:
        return 999
    else:
        return None

def sum_unmarked(board, marked):
    sum = 0
    for i in range(len(board)):
        for j in range(len(board[i])):
            sum += -1*(int(marked[i][j])-1) * int(board[i][j])
    return sum

def score(board,marked, n):
    return sum_unmarked(board, marked) * int(n)
#nums_test = [99,32,26,71,22]
losing_number = 0
losing_marked = []
losing_board = []
losing_index = 0
loserwon = False
winning_number = 0
for n in nums:
    markeds_updated = []
    for i in range(len(data_boards)):
        marked_updated = []
        for j in range(len(data_boards[i])):
            row_updated = []
            for k in range(len(data_boards[i][j])):
                if int(data_boards[i][j][k]) == int(n):
                    row_updated.append(1)
                else:
                    row_updated.append(data_markeds[i][j][k])
            marked_updated.append(row_updated)
        markeds_updated.append(marked_updated)
    #for l in markeds_updated:
    #    print(l)
    if loserfound:
        if check_loser(markeds_updated) == 999 and loserwon == False:
            loserwon = True
            winning_number = n
            losing_board = data_boards[losing_index]
            losing_marked = markeds_updated[losing_index]

    else:
        if check_loser(markeds_updated) != None:
            print("------------WINNER-----------")
            loserfound = True
            losing_number = n
            losing_index = check_loser(markeds_updated)
        data_markeds = markeds_updated

print(f"losingnumber: {losing_number}, winningnumber = {winning_number}")
#print(data_markeds)

for l in losing_board:
    print(l)

for l in losing_marked:
    print(l)
print(f"sum_unmarked: {sum_unmarked(losing_board, losing_marked)}")
print(f"Magic number: {score(losing_board, losing_marked, winning_number)}")

