f = open('data')

longstring = f.read()

f.close()

splitstring = longstring.split('\n\n')

nums = splitstring[0].split(",")
winnerfound = False
data_boards = []
data_markeds = []
for l in splitstring[1:-1]:
    board = []
    marked = []
    lines = l.split('\n')
    for lin in lines:
        marked.append([0,0,0,0,0])
        board.append(lin.split())
    data_markeds.append(marked)
    data_boards.append(board)

def check_rows(marked):
    won = False
    for i in range(len(marked)):
        wonrow = 1
        for j in range(len(marked[i])):
            if marked[i][j] == 0:
                wonrow = 0
        if wonrow:
            won = True
    return won

def check_cols(marked):
    won = False
    for i in range(len(marked)):
        wonrow = 1
        for j in range(len(marked[i])):
            if marked[j][i] == 0:
                wonrow = 0
        if wonrow:
            won = True
    return won

def check_plate(marked):
    if check_rows(marked) or check_cols(marked):
        pass
        #print(f"board: {marked}, rows: {check_rows(marked)}, cols: {check_cols(marked)}")
    return check_rows(marked) or check_cols(marked)

def check_winner(markeds):
    j = None
    for i in range(len(markeds)):
        if check_plate(markeds[i]):
            j = i
    return j

def check_loser(markeds):
    losers = 0
    last_loser = None
    for i in range(len(markeds)):
        if not check_plate(markeds[i]):
            losers += 1
            last_loser = i
    if losers == 1:
        return last_loser
    else:
        return None

def sum_unmarked(board, marked):
    sum = 0
    for i in range(len(board)):
        for j in range(len(board[i])):
            sum += -1*(int(marked[i][j])-1) * int(board[i][j])
    return sum

def score(board,marked, n):
    return sum_unmarked(board, marked) * int(n)
#nums_test = [99,32,26,71,22]
winning_number = 0
winning_marked = []
winning_board = []
for n in nums:
    if not winnerfound:
      #print(n)
      markeds_updated = []
      for i in range(len(data_boards)):
          marked_updated = []
          for j in range(len(data_boards[i])):
              row_updated = []
              for k in range(len(data_boards[i][j])):
                  if int(data_boards[i][j][k]) == int(n):
                      row_updated.append(1)
                  else:
                      row_updated.append(data_markeds[i][j][k])
              marked_updated.append(row_updated)
          markeds_updated.append(marked_updated)
      #for l in markeds_updated:
      #    print(l)
      if check_winner(markeds_updated) != None:
          print("------------WINNER-----------")
          winnerfound = True
          winning_number = n
          winning_board = data_boards[check_winner(markeds_updated)]
          winning_marked = markeds_updated[check_winner(markeds_updated)]
      data_markeds = markeds_updated
print(f"winningnumber: {winning_number}")
#print(data_markeds)

for l in winning_board:
    print(l)

for l in winning_marked:
    print(l)
print(f"sum_unmarked: {sum_unmarked(winning_board, winning_marked)}")
print(f"Magic number: {score(winning_board, winning_marked, winning_number)}")

